1. background

#### a very simple web server

```ruby
::  python -m http.server 9000
Serving HTTP on 0.0.0.0 port 9000 (http://0.0.0.0:9000/) ...

Keyboard interrupt received, exiting.
127.0.0.1 - - [15/Oct/2020 20:21:41] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [15/Oct/2020 20:21:45] "GET /main.py HTTP/1.1" 200 -
```

#### a less simple web server

python_http_server.py

#### web application frameworks

from https://en.wikipedia.org/wiki/Web_framework  

A web framework (WF) or web application framework (WAF) 

1. support the development of web applications including 
  - web services
  - web resources 
  - web APIs. 
1. provide a standard way to build and deploy web applications.
  1. libraries
    1. security
    1. caching
    1. databasae
  1. templates
    1. page layout
    1. headers
    1. footers
    

https://www.similartech.com/categories/framework

https://en.wikipedia.org/wiki/Comparison_of_web_frameworks#Python

https://www.similartech.com/technologies/django

https://djangostars.com/blog/10-popular-sites-made-on-django/

https://steelkiwi.com/blog/top-10-django-website-examples/





#### model view controller

Many frameworks follow the MVC architectural pattern to separate the data model with business rules from the user interface. This is generally considered a good practice as it modularizes code, promotes code reuse, and allows multiple interfaces to be applied. In web applications, this permits different views to be presented, such as web pages for humans, and web service interfaces for remote applications

Most MVC frameworks follow a push-based architecture also called "action-based". These frameworks use actions that do the required processing, and then "push" the data to the view layer to render the results. Django, Ruby on Rails, Symfony, Spring MVC, Stripes, Sails.js, Diamond, CodeIgniter are good examples of this architecture. 

An alternative to this is pull-based architecture, sometimes also called "component-based". These frameworks start with the view layer, which can then "pull" results from multiple controllers as needed. In this architecture, multiple controllers can be involved with a single view.


#### installing django

https://www.djangoproject.com/download/

```
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 4.19.128-microsoft-standard x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Thu Oct 15 20:34:26 MDT 2020

  System load:  0.08               Processes:             8
  Usage of /:   0.5% of 250.98GB   Users logged in:       0
  Memory usage: 0%                 IPv4 address for eth0: 172.31.142.246
  Swap usage:   0%

0 updates can be installed immediately.
0 of these updates are security updates.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update


This message is shown once once a day. To disable it please create the
/home/ed/.hushlogin file.
ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ python3 -V
Python 3.8.2

ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ wget -N https://bootstrap.pypa.io/get-pip.py
--2020-10-15 20:46:17--  https://bootstrap.pypa.io/get-pip.py
Resolving bootstrap.pypa.io (bootstrap.pypa.io)... 151.101.0.175, 151.101.64.175, 151.101.128.175, ...
Connecting to bootstrap.pypa.io (bootstrap.pypa.io)|151.101.0.175|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1885433 (1.8M) [text/x-python]
Saving to: ‘get-pip.py’

get-pip.py                   100%[============================================>]   1.80M  3.13MB/s    in 0.6s

2020-10-15 20:46:18 (3.13 MB/s) - ‘get-pip.py’ saved [1885433/1885433]

ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ sudo python3 get-pip.py
Collecting pip
  Downloading pip-20.2.3-py2.py3-none-any.whl (1.5 MB)
     |████████████████████████████████| 1.5 MB 418 kB/s
Collecting wheel
  Downloading wheel-0.35.1-py2.py3-none-any.whl (33 kB)
Installing collected packages: pip, wheel
Successfully installed pip-20.2.3 wheel-0.35.1
ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ pip -V
pip 20.2.3 from /home/ed/.local/lib/python3.8/site-packages/pip (python 3.8)

ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ sudo apt install python3-venv
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  python-pip-whl python3.8-venv
The following NEW packages will be installed:
  python-pip-whl python3-venv python3.8-venv
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 1806 kB of archives.
After this operation, 2344 kB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 http://archive.ubuntu.com/ubuntu focal-updates/universe amd64 python-pip-whl all 20.0.2-5ubuntu1.1 [1799 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal-updates/universe amd64 python3.8-venv amd64 3.8.5-1~20.04 [5440 B]
Get:3 http://archive.ubuntu.com/ubuntu focal/universe amd64 python3-venv amd64 3.8.2-0ubuntu2 [1228 B]
Fetched 1806 kB in 2s (913 kB/s)
Selecting previously unselected package python-pip-whl.
(Reading database ... 32149 files and directories currently installed.)
Preparing to unpack .../python-pip-whl_20.0.2-5ubuntu1.1_all.deb ...
Unpacking python-pip-whl (20.0.2-5ubuntu1.1) ...
Selecting previously unselected package python3.8-venv.
Preparing to unpack .../python3.8-venv_3.8.5-1~20.04_amd64.deb ...
Unpacking python3.8-venv (3.8.5-1~20.04) ...
Selecting previously unselected package python3-venv.
Preparing to unpack .../python3-venv_3.8.2-0ubuntu2_amd64.deb ...
Unpacking python3-venv (3.8.2-0ubuntu2) ...
Setting up python-pip-whl (20.0.2-5ubuntu1.1) ...
Setting up python3.8-venv (3.8.5-1~20.04) ...
Setting up python3-venv (3.8.2-0ubuntu2) ...
Processing triggers for man-db (2.9.1-1) ...
ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ python3  -m venv ll_env
ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ source ll_env/bin/activate
(ll_env) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ pip install django==2.2.*
Collecting django==2.2.*
  Downloading Django-2.2.16-py3-none-any.whl (7.5 MB)
     |████████████████████████████████| 7.5 MB 382 kB/s
Collecting pytz
  Downloading pytz-2020.1-py2.py3-none-any.whl (510 kB)
     |████████████████████████████████| 510 kB 20.0 MB/s
Collecting sqlparse>=0.2.2
  Downloading sqlparse-0.4.1-py3-none-any.whl (42 kB)
     |████████████████████████████████| 42 kB 97 kB/s
Installing collected packages: pytz, sqlparse, django
```


```
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ mkdir learning_log
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects$ cd learning_log/
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$ pip install django
Collecting django
  Downloading Django-3.1.2-py3-none-any.whl (7.8 MB)
     |████████████████████████████████| 7.8 MB 8.0 MB/s
Collecting sqlparse>=0.2.2
  Using cached sqlparse-0.4.1-py3-none-any.whl (42 kB)
Collecting pytz
  Using cached pytz-2020.1-py2.py3-none-any.whl (510 kB)
Collecting asgiref~=3.2.10
  Downloading asgiref-3.2.10-py3-none-any.whl (19 kB)
Installing collected packages: sqlparse, pytz, asgiref, django




Successfully installed asgiref-3.2.10 django-3.1.2 pytz-2020.1 sqlparse-0.4.1
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$ django-admin startproject learngin_log .
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$ python manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$ ls
db.sqlite3  learngin_log  manage.py
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$ python manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
October 22, 2020 - 22:57:11
Django version 3.1.2, using settings 'learngin_log.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
[22/Oct/2020 22:57:28] "GET / HTTP/1.1" 200 16351
[22/Oct/2020 22:57:28] "GET /static/admin/css/fonts.css HTTP/1.1" 200 423
[22/Oct/2020 22:57:28] "GET /static/admin/fonts/Roboto-Bold-webfont.woff HTTP/1.1" 200 86184
[22/Oct/2020 22:57:28] "GET /static/admin/fonts/Roboto-Light-webfont.woff HTTP/1.1" 200 85692
[22/Oct/2020 22:57:28] "GET /static/admin/fonts/Roboto-Regular-webfont.woff HTTP/1.1" 200 85876
Not Found: /favicon.ico
[22/Oct/2020 22:57:28] "GET /favicon.ico HTTP/1.1" 404 1978
^C(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$ ls
db.sqlite3  learngin_log  manage.py
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$ ls -l learngin_log/
total 8
-rwxrwxrwx 1 ed ed    0 Oct 22 16:55 __init__.py
drwxrwxrwx 1 ed ed 4096 Oct 22 16:57 __pycache__
-rwxrwxrwx 1 ed ed  401 Oct 22 16:55 asgi.py
-rwxrwxrwx 1 ed ed 3080 Oct 22 16:55 settings.py
-rwxrwxrwx 1 ed ed  754 Oct 22 16:55 urls.py
-rwxrwxrwx 1 ed ed  401 Oct 22 16:55 wsgi.py
(ll_env2) ed@tower10:/mnt/c/Users/efugi/PycharmProjects/python-projects/learning_log$
```

## Additional resources and notes

### the official first app
https://docs.djangoproject.com/en/3.1/intro/tutorial01/

### Django for Everybody (DJ4E) 
https://www.dj4e.com/lessons

### django-admin
https://docs.djangoproject.com/en/2.2/ref/django-admin/

### markdown
https://about.gitlab.com/handbook/markdown-guide/
